redis
=====

Install and configure redis master-slave replication and optional redis-sentinel to handle failover if the master goes down.

**Important:** Keep in mind that this role will restart redis and redis-sentinel at every Ansible execution because redis is changing values dynamically in it's config files `/etc/redis/redis.conf` and `/etc/redis/sentinel.conf` after the service is started.<br>However it is not critical to overwrite them with the provided template config in this role. The services will be restarted and the config files will be changed by redis again.

Requirements
------------

- Tested only on Debian 11 (bullseye)

Role Variables
--------------

The default values for the variables are set in `defaults/main.yml`:
```yml
# defaults file for redis
redis_user: redis
redis_group: "{{ redis_user }}"
redis_dir: /var/lib/redis # Changes currently not supported in this role.
redis_logfile: /var/log/redis/redis-server.log 
redis_loglevel: notice

redis_bind: "127.0.0.1 {{ ansible_default_ipv4.address }}" 
redis_protected_mode: "no" # Required for replication and redis-sentinel
redis_port: 6379

redis_password: "" # Optional: Set password for clients that want to connect to redis

redis_masteruser: "masteruser" # Provide user for master-slave authentication
redis_masterauth_password: "" # Provide password for master-slave authentication

## Replication options for redis slaves
# Set replicaof only on the redis slave nodes and point to master (e.g. "<Master_IP> <Redis_Port>")
redis_replicaof: ""

## Redis sentinel configs
# Set this to true on a host to configure it as a sentinel
redis_sentinel: false
redis_sentinel_protected_mode: "no" #Required for replication and sentinel failover

redis_sentinel_dir: /var/lib/redis # Changes to this variable currently not supported in this role.
redis_sentinel_logfile: /var/log/redis/redis-sentinel.log

redis_sentinel_bind: "{{ ansible_default_ipv4.address }}" # Listen on default ipv4 address
redis_sentinel_port: 26379

# Set this parameters at role execution. All sentinels need to monitor the redis master server (e.g. group variable for all sentinel nodes)
#redis_sentinel_monitor:
#  name: mymaster # identifier for master
#  host: 10.199.34.146 # redis master ip
#  port: 6379 # redis master port
#  quorum: 2 # quorum that needs to be reached to failover to a slave
```

Dependencies
------------

- No Dependencies

Example Inventory
-----------------
```yml
all:
  children:
    redis:
      children:
        redis-master:
          hosts:
            redis-01.example.com:
              ansible_host: 10.199.34.146
        redis-slave:
          hosts:
            redis-02.example.com:
              ansible_host: 10.199.34.147
            redis-03.example.com:
              ansible_host: 10.199.34.148
```

**Group Vars:**

`redis.yml:`
```yml
# General vars for all redis nodes
redis_bind: "127.0.0.1 {{ ansible_default_ipv4.address }}"
redis_masteruser: "masteruser" # Provide user for master-slave authentication
redis_masterauth_password: "SecretPassword1" # Provide password for master-slave authentication
redis_password: "SecretPassword2" # Optional: Set password for clients that want to connect to redis

# Set this to true on a host to configure it as a sentinel
redis_sentinel: true

redis_sentinel_monitor:
  name: mymaster # identifier for master
  host: 10.199.34.146 # redis master ip
  port: 6379 # redis master port
  quorum: 2 # quorum that needs to be reached to failover to a slave
```


`redis-slave:`
```yml
# Vars only for redis slaves
redis_replicaof: "10.199.34.146 6379"
```

Example Playbook
----------------

```yml
# Example: Configure 3 redis nodes with one master, two slaves and install redis-sentinel on all redis nodes to handle failover if master goes down
- hosts: redis
  become: yes
  gather_facts: yes
  roles:
    - role: redis
```

License
-------

GPLv3

Author Information
------------------

Oleg Franko
